const each = (obj, iter) => {
    let value;
    for (let key in obj) {
        value = obj[key];
        iter(value, key, obj);
    }
};

const reachable = (events) => {
    let reachable = {};
    let added;
    do {
        added = false;
        let reach, _state;
        each(events, (state, name) => {
            reach = reachable[name] = reachable[name] || {};
            each(state, (_name, event) => {
                if (!reach[_name]) {
                    reach[_name] = [event];
                    added = true;
                }
            });
            each(state, (_name, event) => {
                _state = reachable[_name];
                each(_state, (path, __name) => {
                    if (!reach[__name]) {
                        reach[__name] = [event].concat(path);
                        added = true;
                    }
                });
            });
        });
    } while (added);
    return reachable;
};

const validateTransitions = (t) => {
    const states = Object.keys(t);
    let state, event;
    for (state of states) {
        for (event of Object.keys(t[state])) {
            if (!states.includes(t[state][event])) {
                throw new Error(`Error: Event [${event}] leads to unknown state [${t[state][event]}]`);
            }
        }
    }
};

let events = {};
let eventsOnce = {};

const pushOrCreate = (v, arr) => {
    if (Array.isArray(arr)) arr.push(v); else arr = [v];
    return arr;
};

const emitEvent = (evt, msg) => {
    if (events[evt]) {
        for (const e of events[evt]) {
            if (msg) e(msg); else e();
        }
    }

    if (eventsOnce[evt]) {
        for(let i = 0; i < eventsOnce[evt].length; i++) {
            if (msg) eventsOnce[evt][i](msg); else eventsOnce[evt][i]();
            eventsOnce[evt].shift();
        }
    }
};

export const M = (initialState, transitions) => {
    try { validateTransitions(transitions); } catch (e) { console.error(e); }
    const on = (evt, cb) => events[evt] = pushOrCreate(cb, events[evt]);
    const once = (evt, cb) => eventsOnce[evt] = pushOrCreate(cb, eventsOnce[evt]);

    emit.on = on;
    emit.once = once;
    emit.state = initialState;
    emit._transitions = transitions;
    emit._graph = reachable(transitions);

    function emit(evt) {
        const nextState = emit._transitions[emit.state][evt];

        if (!canReach(emit.state, nextState, emit._graph)) {
            const err = `Error: "${emit.state}" state has no "${evt}" event.`;
            return emitEvent("error", err);
        }

        const startEvent = `${evt}:enter`;
        const finishEvent = `${evt}:leave`;
        const leaveEvent = `${emit.state}:leave`;
        const enterEvent = `${nextState}:enter`;

        if (!emit.state) return emitEvent(enterEvent);

                                     // e.g.
        emitEvent(startEvent);       // fix:enter
        emitEvent(evt);              // fix
        emitEvent(finishEvent);      // fix:leave
        emitEvent(leaveEvent);       // BROKEN:leave
        emitEvent("*", nextState);   // PAUSED
        emitEvent(enterEvent);       // PAUSED:enter
        emit.state = nextState;
    }

    return emit;
}

const canReach = (curr, next, graph) => {
    if (!next) return false;
    if (!curr) return true;
    const here = graph[curr];
    if (!here || !here[next]) return false;
    return here[next].length === 1;
};
