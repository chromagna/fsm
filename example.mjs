import { M } from "./src/fsm.js";

const m = M("PLAYING", {
    PLAYING: { pause: "PAUSED", stop: "STOPPED", break: "BROKEN" },
    PAUSED: { play: "PLAYING", stop: "STOPPED" },
    STOPPED: { play: "PLAYING" },
    BROKEN: { fix: "PAUSED" },
});

m.on("error", (err) => console.log(err));
m.on("*", (state) => console.log("ENTERED STATE:", state));
m.once("break", () => console.log("Oh noooo, it broke!"));
m.on("pause", () => console.log("EVENT: pause"));

m("play"); // err - cant play from PLAYING
m("stop");
m("play");
m("break");
m("play"); // err - cant play from BROKEN
m("pause"); // err - cant pause from BROKEN
m("fix");
m("stop");

// m.state === "STOPPED"
